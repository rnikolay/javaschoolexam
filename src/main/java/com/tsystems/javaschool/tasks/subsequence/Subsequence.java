package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        boolean isAlright = true;
        int cursor = 0;
        for (int iterX = 0; iterX < x.size(); iterX++) {
            if (!isAlright) break;
            isAlright = false;
            for (int iterY = cursor; iterY < y.size(); iterY++) {
                if (x.get(iterX).equals(y.get(iterY))) {
                    isAlright = true;
                    cursor = iterY + 1;
                    break;
                }
            }
        }
        return isAlright;
    }
}


              
